function procuraSubstr (str, sub) {
  var count = 0;
  var result = 0;
  for (var i = 0; i<str.length; i++) {
    for (var j = 0; j<str.length; j++) {
      count = 0;
      if (i < j) {
        count += (str.substring(i, j) == sub) ? 1 : 0;
        if (count == 1 && result == 0) {
          result = i;
        }
      }
    }
  }
  if (result == 0) {
    result = -1;
  }
  return result;
}
var s = 'eu gosto muito de ouvir um metal pesado'
console.log(procuraSubstr(s, 'metal'));
console.log(procuraSubstr(s, 'sertanejo'));
