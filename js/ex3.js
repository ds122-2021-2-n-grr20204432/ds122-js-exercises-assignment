function somaArray(array) {
    var soma = 0;
    for (var i = 0; i < array.length; i++) {
        soma += array[i];
    }
    return soma;
}
var a = [1,2,3,5,6,7,32];
console.log(somaArray(a));