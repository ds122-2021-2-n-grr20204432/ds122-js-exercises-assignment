function contaLetras (s, l) {
  var count = 0;
  for (var i = 0; i < s.length; i++) {
    count += (s.charAt(i) == l) ? 1 : 0;
  }
  return count;
}
var str = "Ola, como voce esta?";
console.log(contaLetras(str, 'a'));
