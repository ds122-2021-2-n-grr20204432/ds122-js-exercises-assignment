//Primeira solução
function printTabuleiro (tamanho) {
    var linha = "";
    var meio, i;

    if (tamanho % 2 == 0) {
        meio = tamanho / 2;
    } else {
        meio = (tamanho / 2) + 0,5;
    }

    for (i=1; i<=tamanho; i++) {
        if (i % 2 == 0) {
            linha += "# ".repeat(meio) + "\n";
        } else {
            linha += " #".repeat(meio) + "\n";
        }
    }
    return linha;
}
var tamanho = 8; //tamanho X tamanho: tabuleiro quadrado.
console.log(printTabuleiro(tamanho));

//Melhor solução
function printTabuleiro2 (tamanho) {
    var linha = "";
    for (i=1; i<=tamanho; i++) {
        for (j=1; j<=tamanho; j++) {
            linha += (j + i) % 2 ? '#' : ' ';
        }
    linha += '\n';
    }
    return linha;
}
tamanho = 8;
console.log(printTabuleiro2(tamanho));